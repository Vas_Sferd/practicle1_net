#pragma once


#include "ibtree.h"


namespace practicle1net {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::OpenFileDialog^ openTree;
	protected:
	protected:
	
	/* �������� ������� */

	private: System::Windows::Forms::SaveFileDialog^ saveTree;

	private: System::Windows::Forms::OpenFileDialog^ openImage;


	/* ������ */
	private: System::Windows::Forms::Button^ ExitButton;
	private: System::Windows::Forms::Button^ OpenTreeButton;
	private: System::Windows::Forms::Button^ SaveTreeButton;
	private: System::Windows::Forms::Button^ SortButton;
	
	private: System::Windows::Forms::Button^ DrawerButton;
	private: System::Windows::Forms::Button^ HTMLReportButton;

	/* ������ */
	private: String^ original_string;
	private: String^ received_srting;

	/* ���� Box */
	private: System::Windows::Forms::TextBox^ textBox1;

	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->ExitButton = (gcnew System::Windows::Forms::Button());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->OpenTreeButton = (gcnew System::Windows::Forms::Button());
			this->SaveTreeButton = (gcnew System::Windows::Forms::Button());
			this->SortButton = (gcnew System::Windows::Forms::Button());
			this->openTree = (gcnew System::Windows::Forms::OpenFileDialog());
			this->saveTree = (gcnew System::Windows::Forms::SaveFileDialog());
			this->DrawerButton = (gcnew System::Windows::Forms::Button());
			this->HTMLReportButton = (gcnew System::Windows::Forms::Button());
			this->openImage = (gcnew System::Windows::Forms::OpenFileDialog());
			this->SuspendLayout();
			// 
			// openTree
			// 
			this->openTree->FileName = L"openFileDialog1";
			this->openTree->Filter = L"Txt files (*.txt)|*.txt|All files(*.*)|*.*";
			// 
			// openImage
			// 
			this->openImage->FileName = L"openFileDialog2";
			this->openImage->Title = "������� ����������� ��� HTML-������";
			this->openImage->Filter = "Image Files(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|All files (*.*)|*.*";
			// 
			// ExitButton
			// 
			this->ExitButton->Location = System::Drawing::Point(12, 276);
			this->ExitButton->Name = L"ExitButton";
			this->ExitButton->Size = System::Drawing::Size(75, 23);
			this->ExitButton->TabIndex = 0;
			this->ExitButton->Text = L"Exit";
			this->ExitButton->UseVisualStyleBackColor = true;
			this->ExitButton->Click += gcnew System::EventHandler(this, &MyForm::ExitButton_Click);
			// 
			// OpenTreeButton
			// 
			this->OpenTreeButton->Location = System::Drawing::Point(102, 276);
			this->OpenTreeButton->Name = L"OpenTreeButton";
			this->OpenTreeButton->Size = System::Drawing::Size(75, 23);
			this->OpenTreeButton->TabIndex = 2;
			this->OpenTreeButton->Text = L"Open";
			this->OpenTreeButton->UseVisualStyleBackColor = true;
			this->OpenTreeButton->Click += gcnew System::EventHandler(this, &MyForm::button2_Click);
			// 
			// SaveTreeButton
			// 
			this->SaveTreeButton->Location = System::Drawing::Point(183, 276);
			this->SaveTreeButton->Name = L"SaveTreeButton";
			this->SaveTreeButton->Size = System::Drawing::Size(75, 23);
			this->SaveTreeButton->TabIndex = 3;
			this->SaveTreeButton->Text = L"Save";
			this->SaveTreeButton->UseVisualStyleBackColor = true;
			this->SaveTreeButton->Click += gcnew System::EventHandler(this, &MyForm::button3_Click);
			// 
			// SortButton
			// 
			this->SortButton->Location = System::Drawing::Point(264, 276);
			this->SortButton->Name = L"SortButton";
			this->SortButton->Size = System::Drawing::Size(75, 23);
			this->SortButton->TabIndex = 4;
			this->SortButton->Text = L"Sort";
			this->SortButton->UseVisualStyleBackColor = true;
			this->SortButton->Click += gcnew System::EventHandler(this, &MyForm::button4_Click);
			// 
			// DrawerButton
			// 
			this->DrawerButton->Location = System::Drawing::Point(345, 276);
			this->DrawerButton->Name = L"DrawerButton";
			this->DrawerButton->Size = System::Drawing::Size(87, 23);
			this->DrawerButton->TabIndex = 5;
			this->DrawerButton->Text = L"Drawer\r\n";
			this->DrawerButton->UseVisualStyleBackColor = true;
			this->DrawerButton->Click += gcnew System::EventHandler(this, &MyForm::button5_Click);
			// 
			// HTMLReportButton
			// 
			this->HTMLReportButton->Location = System::Drawing::Point(345, 246);
			this->HTMLReportButton->Name = L"HTMLReportButton";
			this->HTMLReportButton->Size = System::Drawing::Size(87, 23);
			this->HTMLReportButton->TabIndex = 6;
			this->HTMLReportButton->Text = L"HTML";
			this->HTMLReportButton->UseVisualStyleBackColor = true;
			this->HTMLReportButton->Click += gcnew System::EventHandler(this, &MyForm::button6_Click);
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(12, 12);
			this->textBox1->Multiline = true;
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(327, 254);
			this->textBox1->TabIndex = 1;
			this->textBox1->TextChanged += gcnew System::EventHandler(this, &MyForm::textBox1_TextChanged);
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(440, 314);
			this->Controls->Add(this->HTMLReportButton);
			this->Controls->Add(this->DrawerButton);
			this->Controls->Add(this->SortButton);
			this->Controls->Add(this->SaveTreeButton);
			this->Controls->Add(this->OpenTreeButton);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->ExitButton);
			this->Name = L"MyForm";
			this->Text = L"MyForm";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	
	private: System::Void ExitButton_Click(System::Object^ sender, System::EventArgs^ e) { // �����
		Close();
	}
	private: System::Void textBox1_TextChanged(System::Object^ sender, System::EventArgs^ e) { // �����
		String^ string1 = textBox1->Text;
	}
	private: System::Void button2_Click(System::Object^ sender, System::EventArgs^ e) {	// ������� ����
		if (openTree->ShowDialog() == Windows::Forms::DialogResult::OK)
			this->textBox1->Text = IO::File::ReadAllText(openTree->FileName);
	}
	private: System::Void button3_Click(System::Object^ sender, System::EventArgs^ e) { // ������ � ����
		saveTree->ShowDialog();
		IO::File::WriteAllText(saveTree->FileName, textBox1->Text);
	}
	private: System::Void button4_Click(System::Object^ sender, System::EventArgs^ e) {	// ����� � �������
		original_string = gcnew String(textBox1->Text);	// ��������� ������������ �����
		String^ log = gcnew String("Input:\n");			// ������ �������

		/* �������� */
		array<String^>^ nums = original_string->Split(' ');
		array<int>^ massiv = gcnew array<int>(nums->Length);

		/* ������� */
		for (int i = 0; i < nums->Length; ++i) {
			massiv[i] = Int32::Parse(nums[i]);
			log += massiv[i].ToString() + "\n";
		}

		// ������������� ������
		ibtree_t * ibtree = mkIbtree(massiv[0]);
		for ( int i = 1; i < massiv->Length; ++i)
		{
			ibtappend( &ibtree, massiv[i]);
		}

		/* ����� ������ */
		chain_t * result = mkChain(massiv->Length);
		ibtDepthFirstSearch( ibtree, result);

		/* ����� */
		received_srting = gcnew String("");				// ��������������� �����
		log += "Output:\n";
		for (int i = 0; i < result->count; ++i)
		{
			for (int j = 0; j < result->nodes[i]->weight; ++j)
			{
				received_srting += result->nodes[i]->value.ToString() + " ";
				log += result->nodes[i]->value.ToString() + "\n";
			}
		}
		
		IO::File::WriteAllText("sort_log.txt", log);
		textBox1->Text = received_srting;

		freeChain(result);
		freeIbtree(ibtree);
	}
	
	private: System::Void button5_Click(System::Object^ sender, System::EventArgs^ e) {	// ����������� ��������
		String^ msg = "�� ������ ������� ����������� ��������?";
		String^ cap = "������ �� �������� ������������ ���������";
		MessageBoxButtons buttons = MessageBoxButtons::YesNo;
		System::Windows::Forms::DialogResult result;
		result = MessageBox::Show(this, msg, cap, buttons);

		if ( result == System::Windows::Forms::DialogResult::Yes)
		{
			Form ^ imageform = gcnew ImageForm();
			imageform->ShowDialog();
		}
	}
	private: System::Void button6_Click(System::Object^ sender, System::EventArgs^ e) {	// ���������� html ����� � �������� ��������		
		if (openImage->ShowDialog() == Windows::Forms::DialogResult::OK)
		{
			System::String^ report = gcnew String(
				/* ������ Html-����� */
				"<!DOCTYPE html><html>\n<head></head><body>"			
				"���������� �������...<br />"
				"������ �� ����������:<br />"
				+ original_string + "<br />"
				"������ ����� ����������:<br />"
				+ received_srting + "<br />"

				/* ��������� ����������� */
				"����������� �����������:<br />"
				"<img src=file:///"
				+ openImage->FileName +
				" alt=\"����������� �� �������\"><br />"
				"</body></html>"
				);	// ����� �����
			
			IO::File::WriteAllText("report.html", report);

			WebForm^ webform = gcnew WebForm();
			webform->ShowDialog();
		}
	}
	};
}
