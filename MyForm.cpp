#include <iostream>
#include <Windows.h>

#include "ImageForm.h"
#include "WebForm.h"

#include "MyForm.h"

using namespace System;
using namespace System::IO;
using namespace System::Windows::Forms;
using namespace practicle1net;

[STAThread]
int WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);
	MyForm form;
	Application::Run(%form);

	return 0;
}