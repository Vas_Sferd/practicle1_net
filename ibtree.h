/*
 * ibtree.h
 * 
 * Copyright 2020 Vas Sferd <vassferd@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#ifndef _IBTREE_H
#define _IBTREE_H


//======================================================================

typedef struct ibtree__t
{
	// Связи
	struct ibtree__t * left;
	struct ibtree__t * right;
	
	// Данные
	int weight;	// Вес (кол-во совпадений)
	int value;	// Число
	
	// Высота данного узла
	int height;
	
} ibtree_t;

typedef struct
{
	ibtree_t ** nodes;	//	Массив указателей на узлы дерева
	int count;			//	Число узлов дерева в цепи (расширяется)
	int max_count;		//	Максимальное число узлов в цепи (не расширяется) 

} chain_t;


//======================================================================

ibtree_t * mkIbtree( int value);
int ibtappend( ibtree_t ** ibtree_ptr, int value);
void freeIbtree(ibtree_t * ibtree);
void ibtDepthFirstSearch( ibtree_t * ibtree, chain_t * result);	// Îáõîä ñ âîçâðàòîì ñîðòèðîâàííîé öåïè


//======================================================================

chain_t * mkChain( int max_count);								// Ñîçäàíèå öåïè
int pushToChain( chain_t * chain, ibtree_t * node);				// Äîáàâëåíèå
ibtree_t * popFromChain( chain_t * chain);
void freeChain( chain_t * chain);								// Óäàëåíèå öåïè


#endif
