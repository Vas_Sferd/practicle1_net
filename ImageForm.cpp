#include <iostream>
#include <Windows.h>

#include "ImageForm.h"

using namespace System;
using namespace System::IO;
using namespace System::Windows::Forms;
using namespace practicle1net;

[STAThread]
void Main(HINSTANCE, HINSTANCE, LPSTR, int)
{
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);
	ImageForm form;
	Application::Run(%form);
}