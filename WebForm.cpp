#include <iostream>
#include <Windows.h>

#include "WebForm.h"

using namespace System;
using namespace System::IO;
using namespace System::Windows::Forms;
using namespace practicle1net;

[STAThread]
int Main(HINSTANCE, HINSTANCE, LPSTR, int)
{
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);
	WebForm form;
	Application::Run(%form);

	return 0;
}