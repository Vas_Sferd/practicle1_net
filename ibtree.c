/*
 * ibtree.c
 * 
 * Copyright 2020 Vas Sferd <vassferd@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include <malloc.h>
#include <math.h>
#include <stdio.h>
#include <stdbool.h>

#include "ibtree.h"

// Создание дерева
ibtree_t * mkIbtree(int value)
{
	ibtree_t * ibtree = (ibtree_t *)malloc(sizeof(ibtree_t));
	
	ibtree->left = NULL;
	ibtree->right = NULL;
	
	ibtree->value = value;
	ibtree->weight = 1;
	
	ibtree->height = 1;
	
	return ibtree;
}

// Высота узла
static inline int ibtHeight(ibtree_t * ibtree)
{
	if (! ibtree) return -1;

	int left_height = (ibtree->left)
		? ibtree->left->height
		: 0;

	int right_height = (ibtree->right)
		? ibtree->right->height
		: 0;

	return 1 +
		(
			( left_height > right_height)
			? left_height
			: right_height
		);
}

// Фактор баланса
static inline int bfactor(ibtree_t * ibtree)
{
	if (! ibtree)
	{
		return 0;
	}
	else
	{
		int left_height = ibtHeight(ibtree->left);
		int right_height = ibtHeight(ibtree->right); 
		return right_height - left_height;
	}
}

// Поворот влево
static ibtree_t * rotateleft(ibtree_t * p)
{
	ibtree_t * q = p->right;
	p->right = q->left;
	q->left = p;
	p->height = ibtHeight(p);
	q->height = ibtHeight(q);
	return q;
}

// Поворот вправо
static ibtree_t * rotateright(ibtree_t * p)
{	
	ibtree_t * q = p->left;
	p->left = q->right;
	q->right = p;
	p->height = ibtHeight(p);
	q->height = ibtHeight(q);
	return q;
}

// Балансировка дерева
static ibtree_t * ibtBalance(ibtree_t * ibtree)
{
	if (! ibtree) return NULL;			// Выход для пустого дерева

	switch (bfactor(ibtree))
	{
	case 2:
		if (bfactor(ibtree->right) < 0)
			ibtree->right = rotateright(ibtree->right);
		return rotateleft(ibtree);

	case -2:
		if  (bfactor(ibtree->left) > 0)
			ibtree->left = rotateleft(ibtree->left);
		return rotateright(ibtree);

	default:
		return ibtree;
	}
}

// Балансировка после добавления
static ibtree_t * ibtChainBalance(ibtree_t * ibtree, chain_t * way)
{
	/*
	 * Балансировка по цепи. Начинаем с родителя свежевставленного узла
	 */
	
	ibtree_t ** ptr_to_current;
	ibtree_t * current;
	int old_height;

	for (int i = way->count - 1; i >= 0; --i)
	{
		current = way->nodes[i];
		old_height = current->height;				// Первоначальная высота
		
		current->height = ibtHeight(current);		// Пересчитываем  высоту текущего узла
		
		ptr_to_current = (i == 0)					// Получаем указатель на указатель вышестоящей ноды
			? &ibtree
			: (way->nodes[i - 1]->value > current->value)
				? &(way->nodes[i - 1]->left)
				: &(way->nodes[i - 1]->right);
		
		*ptr_to_current = ibtBalance(current);		// Балансировка узла
		current = *ptr_to_current;
		
		if (current->height == old_height)			// Выход, если больше не требуется балансировка
		{
			break;				
		}
	}

	return ibtree;
}

// Добавление в дерево
int ibtappend(ibtree_t ** ibtree_ptr, int value)
{	
	enum	
	{
		SUCCSES = 0,
		IN_WORK,
		ERROR = -1
	} status = IN_WORK;								// Статус выполнения
	
	if (! ibtree_ptr)								// Проверяем корректность указателя
	{
		status = ERROR;
	}			
	else
	{
		ibtree_t * ibtree = *ibtree_ptr;			// Получаем указатель на дерево
		
		if (! ibtree)
		{
			*ibtree_ptr = mkIbtree(value);
			status = SUCCSES;			// Для пустого дерева
		}
		else
		{
			/* Создаём вспомогательные переменные */
			chain_t * way = mkChain(ibtree->height);
		
			ibtree_t * current = ibtree;						// Текущий элемент
			ibtree_t ** next;									// 

			/* Основный цикл */
			while (status == IN_WORK)
			{			
				if (current->value == value)					// Поиск совпадения
				{
					++current->weight;
					status = SUCCSES;
				}
				else											// Нахождение пустой позиции и добавление
				{	
					/* Добавляем текущий элемент в стек пути*/
					pushToChain(way, current);
				
					/* Следующий узел дерева */
					next = (value >= current->value)
					? &(current->right)
					: &(current->left);
				
					if (! *next)	// Вставка нового узла
					{
						*next = mkIbtree(value); 
						status = SUCCSES;
					
						/* Балансируем если необходимо */
						if (! (current->left && current->right))	
						{	
							// Обновляем дерево
							ibtree = ibtChainBalance(ibtree, way);
							*ibtree_ptr = ibtree;
						}
					}
					else
					{
						/* Подготовка к слудующей итерации */
						current = *next;
					}
				}
			}
	
			freeChain(way);
		}
	}
	
	return status;
}

ibtree_t * ibtMinValueNode(ibtree_t * ibtree)
{
	ibtree_t * current = ibtree;

	while (current->left)
	{
		current = current->left;
	}

	return current;
}

ibtree_t * ibtRemoveMin(ibtree_t * ibtree)
{
	if (! ibtree->left)
		return ibtree->right;
	ibtree->left = ibtRemoveMin(ibtree->left);
	return ibtBalance(ibtree);
}

ibtree_t * ibtRemove(ibtree_t * ibtree, int value)
{
	if (! ibtree) return 0;

	if (value < ibtree->value)
		ibtree->left = ibtRemove(ibtree->left, value);
	else if (value > ibtree->value)
		ibtree->right = ibtRemove(ibtree->right, value);
	else
	{
		ibtree_t * q = ibtree->left;
		ibtree_t * r = ibtree->right;
		free(ibtree);
		if (! r) return q;
		ibtree_t * min = ibtMinValueNode(r);
		min->right = ibtRemoveMin(r);
		min->left = q;
		return ibtBalance(min);
	}

	return ibtBalance(ibtree);
}

// Освобождение памяти для дерева
void freeIbtree(ibtree_t * ibtree)
{
	if (ibtree)
	{
		freeIbtree(ibtree->left);
		freeIbtree(ibtree->right);
		free(ibtree);
	}
}

// Поиск в глубину
void ibtDepthFirstSearch( ibtree_t * ibtree, chain_t * result)
{	
	/*	// Нерекурсивная реализация
	if ( ! ibtree) return;
	
	chain_t * chain = mkChain( ibtree->height);
	int value;
	
	ibtree_t * current = ibtree;
	
	while ( 1)
	{	
		printf("[Info] (%s.%d);\n", __FILE__, __LINE__);
		
		// Нахождение минимума
		while ( current->left != NULL)
		{
			pushToChain( chain, current);
			current = current->left;
		}
		
		printf("[Info] (%s.%d);\n", __FILE__, __LINE__);
		
		// Захват минимума
		pushToChain( result, current);
		
		printf("[Info] (%s.%d);\n", __FILE__, __LINE__);
		
		// Подъём наверх
		do
		{
			if ( current)
			{
				value = current->value;
				current = popFromChain( chain);
			}
			
			printf("[Info] (%s.%d);\n", __FILE__, __LINE__);
			
		}	while ( current || ( value < current->value));
		
		printf("[Info] (%s.%d);\n", __FILE__, __LINE__);
		
		// С
		if ( ! current)
		{
			break;	// Выход при возвращении в корень
		}
		else
		{
			pushToChain( result, current);
			
			while ( ! current->right)
			{
				current = popFromChain( chain);
				pushToChain( result, current);
			}	
			
			pushToChain( chain, current);
			current = current->right;
		}
		
		printf("[Info] (%s.%d);\n", __FILE__, __LINE__);
	}
	
	freeChain( chain);
	*/
	
	if (ibtree)
	{
		ibtDepthFirstSearch(ibtree->left, result);		// Обход левого поддерева
		pushToChain(result, ibtree);					// Посещение узла
		ibtDepthFirstSearch(ibtree->right, result);		// Обход правого поддерева
	}
}

// Инициализируем пустую цепь определённого размера
chain_t * mkChain(int max_count)
{
	chain_t * chain = (chain_t *)malloc(sizeof( chain_t));
	
	chain->nodes = (ibtree_t **)malloc(max_count * sizeof(ibtree_t *));
	chain->count = 0;
	chain->max_count = max_count;

	return chain;
}

// Добавить значение в цепь
int pushToChain(chain_t * chain, ibtree_t * node)
{
	if (chain && node && chain->count < chain->max_count)
	{
		chain->nodes[chain->count] = node;
		chain->count = chain->count + 1;

		return 0;
	}
	else
	{
		return -1;
	}
}

// Извлечь указатель на узел из цепи
ibtree_t * popFromChain(chain_t * chain)
{
	if (! chain || chain->count == 0)	return NULL;
	
	ibtree_t * removed = chain->nodes[chain->count - 1];
	--chain->count;
	
	return removed;
}

// Освобождение памяти для цепи (не касаясь узлов)
void freeChain(chain_t * chain)
{
	free(chain->nodes);
	free(chain);

	return;
}

// Следущее поколение
inline
static
ibtree_t ** ibtNextGen(ibtree_t ** gen, size_t gen_index)
{
	size_t in_gen_count = pow(2, gen_index);
	
	// Следующее поколение
	// Число элементов в два раза больше
	ibtree_t **
	next_gen = (ibtree_t **)malloc(2 * in_gen_count * sizeof(ibtree_t *));

	for (
		size_t i = 0, j = 0;
		i < in_gen_count;
		++i
		)
	{
		if (gen[i] != NULL)
		{
			next_gen[j++] = gen[i]->left;
			next_gen[j++] = gen[i]->right;
		}
		else
		{
			next_gen[j++] = NULL;
			next_gen[j++] = NULL;
		}
	}

	return next_gen;
}

// Печать дерева
int ibtreeGraphicDrow(ibtree_t * ibtree, int view_hieght)
{
	enum
	{
		SUCCESS,
		IN_WORK,
		ERROR = -1
	}
	status = IN_WORK;

	// Создаём двумерный массив
	// [Поколение][Значение узла]
	ibtree_t ***
	generations = (ibtree_t ***)malloc(view_hieght * sizeof(ibtree_t **));

	// Инициализируем первое поколение c помощью корня
	generations[0] = &ibtree;

	for (
		int i = 1;
		i < view_hieght && status == IN_WORK;
		++i
		)
	{
		// Следующее поколение
		generations[i] = ibtNextGen(generations[i - 1], i - 1);
	}
	
	for (int i = 0; i < view_hieght; ++i)
	{
		for (int j = view_hieght / 2 + 1; j > i; --j)	// Первоначальный отступ
		{
			printf("\t");
		}
		
		for (int j = 0; j < (int)pow(2, i); ++j)
		{
			if (generations[i][j] != NULL)
			{
				printf("%d:>%-4d\t", j / 2 + 1, generations[i][j]->value);
			}
			else
			{
				printf("%d:>%s\t", j / 2 + 1, "null");
			}
		}
	
		printf("\n");
	}
	
	// Освобождаем память (начинаем со второй ветви)
	for (int i = 1; i < view_hieght; ++i)
	{
		free(generations[i]);
	}
	
	free(generations);
	
	return 0;
}
