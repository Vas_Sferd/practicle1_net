#pragma once

namespace practicle1net {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::IO;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� WebForm
	/// </summary>
	public ref class WebForm : public System::Windows::Forms::Form
	{
	public:
		WebForm(void)
		{
			InitializeComponent();
			OpenBrowser();

			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~WebForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::WebBrowser^ webBrowser1;
	private: System::Windows::Forms::Button^ ExitButton;

	protected:

	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->webBrowser1 = (gcnew System::Windows::Forms::WebBrowser());
			this->ExitButton = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// webBrowser1
			// 
			this->webBrowser1->Location = System::Drawing::Point(0, 3);
			this->webBrowser1->MinimumSize = System::Drawing::Size(20, 20);
			this->webBrowser1->Name = L"webBrowser1";
			this->webBrowser1->Size = System::Drawing::Size(851, 439);
			this->webBrowser1->TabIndex = 0;
			// 
			// ExitButton
			// 
			this->ExitButton->Location = System::Drawing::Point(768, 457);
			this->ExitButton->Name = L"ExitButton";
			this->ExitButton->Size = System::Drawing::Size(75, 26);
			this->ExitButton->TabIndex = 1;
			this->ExitButton->Text = L"Close";
			this->ExitButton->UseVisualStyleBackColor = true;
			this->ExitButton->Click += gcnew System::EventHandler(this, &WebForm::ExitButton_Click);
			// 
			// WebForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(855, 495);
			this->Controls->Add(this->ExitButton);
			this->Controls->Add(this->webBrowser1);
			this->Name = L"WebForm";
			this->Text = L"WebForm";
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void OpenBrowser( void) {		// �������� ��������
		try
		{
			webBrowser1->DocumentText = File::ReadAllText("report.html");
		}
		catch ( const NullReferenceException^ e)
		{
			webBrowser1->Navigate("Error.html");	// ��������� ������
		}
	}
	private: System::Void ExitButton_Click(System::Object^ sender, System::EventArgs^ e) { // �����
		Close();
	}
};
}