#pragma once

namespace practicle1net {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� ImageForm
	/// </summary>
	public ref class ImageForm : public System::Windows::Forms::Form
	{
	public:
		ImageForm(void)
		{
			InitializeComponent();
			
			/* �������������� graphicGDI */
			this->graphicGDI = Graphics::FromImage(pictureBox1->Image);
			this->graphicGDI->Clear(System::Drawing::Color::White);	
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~ImageForm()
		{
			if (components)
			{
				delete components;
			}
		}
	protected:
	
	/* ����������� */
	private: System::Windows::Forms::PictureBox^ pictureBox1;
	
	/* �������� ������� */
	private: System::Windows::Forms::SaveFileDialog^ saveImage;
	private: System::Windows::Forms::OpenFileDialog^ openImage;

	/* ������ */
	private: System::Windows::Forms::Button^ ExitFromGE;
	private: System::Windows::Forms::Button^ OpenFileButton;
	private: System::Windows::Forms::Button^ SaveFileButton;
	private: System::Windows::Forms::Button^ ClearButton;

	/* ���������������� ���������� */
	private: bool Draw_f;				// ���� ���������
	private: Graphics^ graphicGDI;		// ������� ��� pictureBox1

	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^ resources = (gcnew System::ComponentModel::ComponentResourceManager(ImageForm::typeid));
			this->ExitFromGE = (gcnew System::Windows::Forms::Button());
			this->OpenFileButton = (gcnew System::Windows::Forms::Button());
			this->SaveFileButton = (gcnew System::Windows::Forms::Button());
			this->ClearButton = (gcnew System::Windows::Forms::Button());
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->saveImage = (gcnew System::Windows::Forms::SaveFileDialog());
			this->openImage = (gcnew System::Windows::Forms::OpenFileDialog());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			this->SuspendLayout();
			// 
			// ExitFromGE
			// 
			this->ExitFromGE->Location = System::Drawing::Point(12, 226);
			this->ExitFromGE->Name = L"ExitFromGE";
			this->ExitFromGE->Size = System::Drawing::Size(75, 23);
			this->ExitFromGE->TabIndex = 0;
			this->ExitFromGE->Text = L"Exit\r\n";
			this->ExitFromGE->UseVisualStyleBackColor = true;
			this->ExitFromGE->Click += gcnew System::EventHandler(this, &ImageForm::ExitFromGE_Click);
			// 
			// OpenFileButton
			// 
			this->OpenFileButton->Location = System::Drawing::Point(174, 226);
			this->OpenFileButton->Name = L"OpenFileButton";
			this->OpenFileButton->Size = System::Drawing::Size(75, 23);
			this->OpenFileButton->TabIndex = 3;
			this->OpenFileButton->Text = L"Open";
			this->OpenFileButton->UseVisualStyleBackColor = true;
			this->OpenFileButton->Click += gcnew System::EventHandler(this, &ImageForm::OpenFileButton_Click);
			// 
			// SaveFileButton
			// 
			this->SaveFileButton->Location = System::Drawing::Point(93, 226);
			this->SaveFileButton->Name = L"SaveFileButton";
			this->SaveFileButton->Size = System::Drawing::Size(75, 23);
			this->SaveFileButton->TabIndex = 4;
			this->SaveFileButton->Text = L"Save";
			this->SaveFileButton->UseVisualStyleBackColor = true;
			this->SaveFileButton->Click += gcnew System::EventHandler(this, &ImageForm::SaveFileButton_Click);
			// 
			// ClearButton
			// 
			this->ClearButton->Location = System::Drawing::Point(255, 226);
			this->ClearButton->Name = L"ClearButton";
			this->ClearButton->Size = System::Drawing::Size(75, 23);
			this->ClearButton->TabIndex = 1;
			this->ClearButton->Text = L"Clear";
			this->ClearButton->UseVisualStyleBackColor = true;
			this->ClearButton->Click += gcnew System::EventHandler(this, &ImageForm::ClearButton_Click);
			// 
			// pictureBox1
			// 
			this->pictureBox1->AccessibleRole = System::Windows::Forms::AccessibleRole::Graphic;
			this->pictureBox1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox1.Image")));
			this->pictureBox1->Location = System::Drawing::Point(12, 12);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(318, 208);
			this->pictureBox1->TabIndex = 2;
			this->pictureBox1->TabStop = false;
			this->pictureBox1->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &ImageForm::pictureBox1_MouseDown);
			this->pictureBox1->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &ImageForm::pictureBox1_MouseMove);
			this->pictureBox1->MouseUp += gcnew System::Windows::Forms::MouseEventHandler(this, &ImageForm::pictureBox1_MouseUp);
			// 
			// saveImage
			// 
			this->saveImage->Filter = L"���� �����������(*.bmp)|*.bmp|All files(*.*)|*.*";
			// 
			// openImage
			// 
			this->openImage->Filter = L"Image Files(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|All files (*.*)|*.*";
			this->openImage->Title = L"�������� �����������";
			// 
			// ImageForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(336, 261);
			this->Controls->Add(this->SaveFileButton);
			this->Controls->Add(this->OpenFileButton);
			this->Controls->Add(this->pictureBox1);
			this->Controls->Add(this->ClearButton);
			this->Controls->Add(this->ExitFromGE);
			this->Name = L"ImageForm";
			this->Text = L"ImageForm";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void ClearButton_Click(System::Object^ sender, System::EventArgs^ e) { // �������
		graphicGDI->Clear(SystemColors::Window);
		pictureBox1->Refresh();
	}
	private: System::Void  pictureBox1_MouseMove(System::Object^ sender, System::Windows::Forms::MouseEventArgs^ e) {
		if (Draw_f == true)
		{
			/* ����	� ������� */
			graphicGDI->FillEllipse(Brushes::Black, e->X, e->Y,  5, 5);
			graphicGDI->DrawImage(pictureBox1->Image, 0, 0, 0, 0);
			pictureBox1->Refresh();
		}
	}
	private: System::Void  pictureBox1_MouseDown(System::Object^ sender, System::Windows::Forms::MouseEventArgs^ e)
	{
		(Draw_f = true);
	}
	private: System::Void pictureBox1_MouseUp(System::Object^ sender, System::Windows::Forms::MouseEventArgs^ e)
	{
		(Draw_f = false);
	}
	private: System::Void OpenFileButton_Click(System::Object^ sender, System::EventArgs^ e) { // ������� �����������
		if (openImage->ShowDialog() == Windows::Forms::DialogResult::OK)
		{
			delete this->graphicGDI;									// ������� �������
			
			Image^ img = Image::FromFile(openImage->FileName);			// ������� �����������
			pictureBox1->Image = gcnew Bitmap(img);						// ����������� �����������
			delete img;													// �������� �����
			
			this->graphicGDI = Graphics::FromImage(pictureBox1->Image);	// ������������ ������� �������
		}
	}
	private: System::Void SaveFileButton_Click(System::Object^ sender, System::EventArgs^ e) { // ��������� 
		if (saveImage->ShowDialog() == Windows::Forms::DialogResult::OK) {
			Bitmap^ bmp = gcnew Bitmap(pictureBox1->Image);
			bmp->Save(saveImage->FileName, System::Drawing::Imaging::ImageFormat::Bmp);
			delete bmp;
		}
	}
	private: System::Void ExitFromGE_Click(System::Object^ sender, System::EventArgs^ e) {	// �����
		Close();
	}
};
}
